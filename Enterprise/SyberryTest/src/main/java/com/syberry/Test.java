package com.syberry;

public class Test {
    public static void hello_Syberry(String name) {
        System.out.println("Hello, Syberry, my name is " + name + " and I'm ready for tech interview");
    }

    public static void main(String[] args) {
        String name = "Artem";
        hello_Syberry(name);
    }
}

